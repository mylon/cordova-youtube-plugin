var Youtube = {

    open: function (param, successCallback, errorCallback) {
        if (param.mode == "youtube") {
            this.openYoutube(param, successCallback, errorCallback);
        }
        else if (param.mode == "movie") {
            this.openMovie(param, successCallback, errorCallback);
        }
    },
    openYoutube: function (param, successCallback, errorCallback) {
        Cordova.exec(successCallback, errorCallback, 'AdfitYoutube', 'openYoutube', [param]);
    },
    openMovie: function (param, successCallback, errorCallback) {
        Cordova.exec(successCallback, errorCallback, 'AdfitYoutube', 'openMovie', [param]);
    }
};

if (!window.plugins) {
    window.plugins = {};
}
if (!window.plugins.Youtube) {
    window.plugins.Youtube = Youtube;
}

if (module.exports) {
    module.exports = Youtube;
}


