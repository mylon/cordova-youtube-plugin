//
//  AdfitYoutube.m
//  AdfitYoutube
//
//  Created by Kittipong Kulapruk on 5/8/2557 BE.
//  Copyright (c) 2557 Kittipong Kulapruk. All rights reserved.
//

#import "AdfitYoutube.h"

@implementation AdfitYoutube

- (void) openMovie:(CDVInvokedUrlCommand *) command {
    self.callbackId = command.callbackId;
    NSMutableDictionary* options = [command.arguments objectAtIndex:0];
    NSString * videoId = [options objectForKey:@"youtubeId"] ;
    adfit =  [[AdfitYoutubeViewController alloc] init];
    [adfit setYoutubeDelegate:self];
    [adfit setMode:[options objectForKey:@"mode"]];
    [adfit setMovieUrl:[options objectForKey:@"url"]];
    [adfit setCancelLabel:[options objectForKey:@"cancelLabel"]];
    [self.viewController.view addSubview:adfit.view];
}

- (void) openYoutube:(CDVInvokedUrlCommand*)command{
    self.callbackId = command.callbackId;
    NSMutableDictionary* options = [command.arguments objectAtIndex:0];
    NSString * videoId = [options objectForKey:@"youtubeId"] ;
    adfit =  [[AdfitYoutubeViewController alloc] init];
    [adfit setMode:[options objectForKey:@"mode"]];    
    [adfit setYoutubeDelegate:self];
    [adfit setYoutubeId:videoId];
    [adfit setCancelLabel:[options objectForKey:@"cancelLabel"]];
    [self.viewController.view addSubview:adfit.view];
}

- (void) closeYoutubeWithFinish:(BOOL) finish{
    
    if(finish){
        
        CDVPluginResult *commandResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:@"ended"];
    
        [self.commandDelegate sendPluginResult:commandResult callbackId:self.callbackId];
    }
    else{
        CDVPluginResult *commandResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:@"not ended"];
        [self.commandDelegate sendPluginResult:commandResult callbackId:self.callbackId];
    }
    
    
    
    [adfit setYoutubeDelegate:nil];
    [adfit.view removeFromSuperview];
    adfit = nil;

    /*
    [self.viewController dismissViewControllerAnimated:YES completion:^{
            
    }];
    */
    

    //[self performSelectorOnMainThread:@selector(closeonMainThread) withObject:nil waitUntilDone:NO];
}

-(void) closeonMainThread{
    //[adfit dismissModalViewControllerAnimated:NO];
    //[self.viewController dismissViewControllerAnimated:YES completion:nil];
    [adfit.view removeFromSuperview];
    //[adfit setYoutubeDelegate:nil];
    //adfit = nil;
}

@end
