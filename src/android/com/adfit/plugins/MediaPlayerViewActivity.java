package com.adfit.plugins;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Resources;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaPlayer.OnPreparedListener;
import android.media.MediaPlayer.OnSeekCompleteListener;
import android.net.Uri;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.VideoView;

public class MediaPlayerViewActivity extends Activity implements
        OnCompletionListener, OnSeekCompleteListener, OnTouchListener,
        OnPreparedListener {

    private VideoView view;

    public void onCreate(Bundle b) {
        super.onCreate(b);

        String packageName = getApplication().getPackageName();
        Resources resources = getApplication().getResources();
        setContentView(resources.getIdentifier("adfitmediaplayer", "layout", packageName));

        String urlString = "";
        Bundle e = getIntent().getExtras();
        if (e != null) {
            urlString = e.getString("movieUrl");
        }

        view = (VideoView) findViewById(resources.getIdentifier("mainvideoview", "id", packageName));
        view.setOnCompletionListener(this);
        view.setOnPreparedListener(this);
        view.setOnTouchListener(this);

        this.playClip(urlString);

        view.start();
    }

    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
        String urlString = null;
        Bundle e = getIntent().getExtras();
        if (e != null) {
            urlString = e.getString("movieUrl");
        }
        playClip(urlString);
    }

    public void stopPlaying() {
        view.stopPlayback();
        this.finish();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        AdfitYoutube adfitYt = AdfitYoutube.getInstance();
        adfitYt.sendCallback(false);
        this.finish();
    }

    protected boolean playClip(String urlString) {
        if (urlString == null) {
            return false;
        }
        view.setVideoURI(Uri.parse(urlString));
        return true;
    }

    @Override
    public void onSeekComplete(MediaPlayer mp) {

    }

    @Override
    public void onCompletion(MediaPlayer mp) {
        AdfitYoutube adfitYt = AdfitYoutube.getInstance();
        adfitYt.sendCallback(true);
        finish();
    }

    @Override
    public void onPrepared(MediaPlayer mp) {
        mp.setLooping(false);
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        return false;
    }

}
